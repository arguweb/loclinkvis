# Location Linker and Visualizer

LocLinkVis is able to detect location names (toponyms) in unstructured text, and links them to the corresponding Open Street Map features.
It is not fool-proof, and there may be inaccuracies in the output it generates.

LocLinkVis is most notably used to provide location-based access to Dutch local government data, through the [WaarOverheid](https://www.waaroverheid.nl) app.

## Running LocLinkVis

Requires [Docker](https://docs.docker.com/install/) and [Docker Compose](https://docs.docker.com/compose/install/) to be installed.

Download the latest release via: https://bitbucket.org/aolieman/loclinkvis/downloads/

Alternatively, clone the git repository:
```bash
git clone git@bitbucket.org:aolieman/loclinkvis.git
```

Run the location linking service with docker-compose:
```bash
docker-compose up
```

To rebuild the docker container after modifying the source code, use the `build` command.
```bash
docker-compose build
```

## Loading OSM data

LocLinkVis isn't able to do much until it has access to a database filled with Open Street Map data.
A third-party tool can be used to ingest OSM dumps into MongoDB:
https://github.com/larroy/osmcompiler

Please follow the instructions provided there to load one or multiple dumps into the database.
If there is sufficient demand, I'll consider creating a docker container to make this gazetteer loading step easier.

## Manual installation
In cloned repository (optional active virtualenv):
```bash
pip install -r requirements.txt
pip install -e .
```

For Anaconda users, the `shapely` package may not install correctly with pip.
It can be installed from a [`conda` package](https://binstar.org/scitools/shapely) instead.

If you encounter any issues with Anaconda and `shapely`, try:
```bash
pip uninstall shapely
conda install -c https://conda.binstar.org/scitools shapely
```

## License
Copyright (C) 2014  Universiteit van Amsterdam  
Copyright (C) 2014  Alex Olieman  
Copyright (C) 2017  TinQwise Stamkracht  

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
