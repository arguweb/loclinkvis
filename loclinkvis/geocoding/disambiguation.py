import logging
import re
import random
import nltk
from functools import partial
from collections import defaultdict

from loclinkvis.gazetteer.models import Feature
from loclinkvis.settings import TOKENIZER_MODEL
from spatial_analysis import llv_distance_ranking


logger = logging.getLogger(__name__)


class Disambiguator(object):

    def __init__(self):
        self.tokenizer = nltk.data.load(TOKENIZER_MODEL)
        self.get_featureset = partial(get_featureset, tokenizer=self.tokenizer)
        self.ttc = train_topo_type_classifier(self.tokenizer)
        self.get_candidates_per_toponym = get_candidates_per_toponym

    def disambiguate_toponyms(self, span_name_tuples, input_text, details=False):
        disambiguated_matches = []

        tnym_set = {t[1] for t in span_name_tuples}
        features_per_toponym = collect_features_per_toponym(tnym_set)

        # normalize toponyms with unambiguous geotypes
        norm_text = input_text
        for tnym, candidates in features_per_toponym.iteritems():
            if len(candidates) == 1:
                geo_type_marker = u'*{}*'.format(candidates[0].get_simple_geotype().upper())
                norm_text = norm_text.replace(tnym, geo_type_marker)

        # topo type classification
        classified_matches = [
            (span, tnym, self.ttc.prob_classify(self.get_featureset(span, norm_text)))
            for span, tnym in span_name_tuples
        ]

        # rank candidates by LLV distance-based PageRank
        llv_distance_rank = llv_distance_ranking(features_per_toponym)

        for span, tnym, pd in classified_matches:
            candidates = features_per_toponym[tnym]
            scored_candidates = sorted([
                (self.compute_score(c, pd, llv_distance_rank), c)
                for c in candidates
            ], key=lambda t: t[0], reverse=True)

            if details:
                prob_dist = {
                    s: pd.prob(s) for s in pd.samples()
                }
                dm = (span, tnym, prob_dist, scored_candidates)
            else:
                score, top_candidate = max(scored_candidates, key=lambda t: t[0])
                dm = (span, tnym, score, top_candidate)

            disambiguated_matches.append(dm)

        return disambiguated_matches

    @staticmethod
    def compute_score(candidate_feat, prob_dist, llv_distance_rank):
        topo_prob = prob_dist.prob(candidate_feat.get_simple_geotype())
        llv_dist_prob = llv_distance_rank[str(candidate_feat._id)]

        return topo_prob * llv_dist_prob


def collect_features_per_toponym(toponyms, municipality_code=None):
    query = {
        'known_names': {'$in': list(toponyms)}
    }
    if municipality_code:
        query['municipalities'] = municipality_code

    features = Feature.collection.find(query)

    features_per_toponym = defaultdict(list)
    for feat in features:
        for name in feat.known_names:
            features_per_toponym[name].append(feat)

    return features_per_toponym


def train_topo_type_classifier(tokenizer, debug=False):
    featuresets = []
    training_path = '../statistical_models/training/topo_type/{}_text_examples.txt'
    topo_classes = (
        'building', 'country', 'municipality', 'neighborhood',
        'province', 'road', 'water'
    )

    for topo_type in topo_classes:
        with open(training_path.format(topo_type), 'r') as f:
            for ex_line in f.readlines():
                if len(ex_line) < 3:
                    continue
                for fs in featureset_from_example(ex_line, tokenizer):
                    featuresets.append((fs, topo_type))

    random.shuffle(featuresets)

    classifier = nltk.NaiveBayesClassifier.train(featuresets)

    if debug:
        return classifier, featuresets
    else:
        return classifier


def featureset_from_example(example_line, tokenizer):
    ex_u = example_line.decode('utf8')
    try:
        tnym, context = ex_u.split('_', 1)
    except ValueError:
        logger.warning(repr(ex_u))
        raise
    for m in re.finditer(tnym, context, re.IGNORECASE | re.U):
        yield get_featureset(m.span(), context, tokenizer)


def get_featureset(topo_span, context_str, tokenizer):
    return make_token_window(
        context_str[:topo_span[0]],
        context_str[topo_span[1]:],
        tokenizer
    )


def is_non_punct(token_obj):
    return token_obj.is_non_punct


def make_token_window(preceding_str, following_str, tokenizer):
    token_window = {}
    p_toks = filter(is_non_punct, tokenizer._tokenize_words(preceding_str))
    f_toks = filter(is_non_punct, tokenizer._tokenize_words(following_str))
    for i, tok in enumerate(list(reversed(p_toks))[:5], 1):
        token_window['token-{}'.format(i)] = tok.type
    for i, tok in enumerate(f_toks[:5], 1):
        token_window['token+{}'.format(i)] = tok.type
    return token_window


def get_candidates_per_toponym(disambiguated_matches):
    postings = defaultdict(list)
    candidates = {}

    for match in disambiguated_matches:
        span, tnym, prob_dist, scored_candidates = match

        # replace features by references for serialization
        scored_candidate_ids = []
        for score, cand in scored_candidates:
            cand_ref = str(cand._id)
            candidates[cand_ref] = cand
            scored_candidate_ids.append((score, cand_ref))

        sorted_prob_dist = sorted(
            prob_dist.items(), key=lambda t: t[1], reverse=True
        )

        postings[tnym].append({
            'span': span,
            'toponym': tnym,
            'geotype_probabilities': sorted_prob_dist,
            'scored_candidates': scored_candidate_ids
        })

    return dict(postings), candidates
