import json

from flask import request

from loclinkvis.municipal import db_collections
from loclinkvis.municipal.annotation import generate_annotations
from loclinkvis.utils.db import get_feature_collection
from loclinkvis.web_app import app

json_headers = {'Content-Type': 'application/json'}

municipal_type_map = {
    'BU': 'neighborhood',
    'WK': 'district',
    'GM': 'municipality',
}
municipal_types = set(municipal_type_map.values())


@app.route('/municipal/<string:cbs_code>/adjacent', methods=['GET'])
def get_adjacent_features(cbs_code):
    municipal_type_prefix = cbs_code[:2]
    municipal_type = municipal_type_map[municipal_type_prefix]
    property_key = 'properties.{}_CODE'.format(municipal_type_prefix)
    detail_level = request.args.get('detail_level', default='closeup')

    db_collection = getattr(
        db_collections,
        '{}_{}'.format(municipal_type, detail_level)
    )
    feature = db_collection.find_one({
        property_key: cbs_code
    })
    adjacent_features = []
    if feature:
        for mtype in municipal_types:
            key_suffix = mtype + 's'
            if mtype == 'municipality':
                key_suffix = 'municipalities'

            adjacent_codes = feature.get('adjacent_{}'.format(key_suffix))
            if adjacent_codes:
                adjacent_key = 'properties.{}_CODE'.format(adjacent_codes[0][:2])
                feature_collection = get_feature_collection({
                    adjacent_key: {'$in': adjacent_codes}
                }, collection=getattr(
                        db_collections,
                        '{}_{}'.format(mtype, detail_level)
                    )
                )
                adjacent_features += feature_collection['features']

    body_str = json.dumps({
        'type': 'FeatureCollection',
        'features': adjacent_features
    })
    return body_str, 200 if feature else 404, json_headers


district_str = 'districts'
neighborhood_str = 'neighborhoods'
child_levels = [district_str, neighborhood_str]
features_by_parent_code_url = (
    '/municipal/<string:cbs_code>/<any('
    + ', '.join(map(repr, child_levels))
    + '):child_type>'
)


@app.route(features_by_parent_code_url, methods=['GET'])
def get_features_by_parent_code(cbs_code, child_type):
    property_key = 'properties.{}_CODE'.format(cbs_code[:2])
    detail_level = request.args.get('detail_level', default='highover')

    feature_collection = get_feature_collection({
        property_key: cbs_code
    }, collection=getattr(
            db_collections,
            '{}_{}'.format(child_type[:-1], detail_level)
        )
    )
    body_str = json.dumps(feature_collection)
    return body_str, 200, json_headers


@app.route('/municipal/<string:cbs_code>', methods=['GET'])
def get_feature_by_code(cbs_code):
    municipal_type_prefix = cbs_code[:2]
    municipal_type = municipal_type_map[municipal_type_prefix]
    property_key = 'properties.{}_CODE'.format(municipal_type_prefix)
    detail_level = request.args.get('detail_level', default='highover')
    db_collection = getattr(
        db_collections,
        '{}_{}'.format(municipal_type, detail_level)
    )

    feature = db_collection.find_one({
        property_key: cbs_code
    })
    if feature:
        feature.pop('_id', None)

    body_str = json.dumps(feature)
    return body_str, 200 if feature else 404, json_headers


@app.route('/municipal/', methods=['GET'])
def get_municipalities():
    municipalities = db_collections.municipality_highover.aggregate([
        {'$group': {'_id': {
            'name': '$properties.GM_NAAM',
            'code': '$properties.GM_CODE',
            'count': '$properties.doc_count',
        }}}
    ])
    municipalities = {'municipalities': [m['_id'] for m in municipalities]}

    body_str = json.dumps(municipalities)
    return body_str, 200, json_headers


@app.route('/localize/', methods=['GET'])
def get_local_area_by_point():
    lon = request.args.get('lon', type=float)
    lat = request.args.get('lat', type=float)
    target_type = request.args.get('type')
    status_code = 200

    if target_type in municipal_types:
        target_collection = getattr(db_collections, '{}_closeup'.format(target_type))
        target_area = target_collection.find_one({
            'geometry': {
                '$geoIntersects': {
                    '$geometry': {
                        'type': 'Point',
                        'coordinates': [lon, lat]
                    }
                }
            }
        })
        if target_area:
            target_area.pop('_id')
            body_str = json.dumps(target_area)
        else:
            status_code = 501
            body_str = json.dumps({
                'lat': 'The provided coordinates are not covered by the loaded database.',
                'lon': 'idem'
            })
    else:
        status_code = 400
        body_str = json.dumps({
            'type': 'target type must be one of {}'.format(municipal_types)
        })

    return body_str, status_code, json_headers


@app.route('/annotate', methods=['POST'])
def annotate_document():
    posted_data = request.get_json()
    missing_parameters = {}
    for parameter in ('text', 'municipality_code'):
        if parameter not in posted_data:
            missing_parameters[parameter] = '{} is a required parameter'.format(parameter)
    if missing_parameters:
        return json.dumps(missing_parameters), 400, json_headers

    municipality_code = posted_data['municipality_code']
    text = posted_data['text']
    annotations = generate_annotations(municipality_code, text)

    # TODO: relevance score
    # per annotation: districts[d_code] += len(postings)
    # normalize all scores by doc length (tokens or chars)
    districts = sorted({
        d_code
        for ann in annotations
        for d_code in ann['districts']
    })
    neighborhoods = sorted({
        n_code
        for ann in annotations
        for n_code in ann['neighborhoods']
    })

    body_str = json.dumps({
        'annotations': annotations,
        'districts': districts,
        'neighborhoods': neighborhoods
    })

    return body_str, 200, json_headers
