from collections import defaultdict
from operator import itemgetter

from loclinkvis.gazetteer.models import Feature
from loclinkvis.municipal import db_collections
from loclinkvis.municipal.recognition import spot_toponyms_in_text
from loclinkvis.settings import MIN_HOOD_SIMILARITY


def generate_annotations(municipality_code, text):
    name_postings_map, _ = spot_toponyms_in_text(municipality_code, text)
    municipal_map = get_districts_and_hoods(municipality_code, name_postings_map.keys())
    hood_candidates = get_features_per_neighborhood(municipality_code, name_postings_map.keys())
    name_annotation_map = select_hoods_and_features(hood_candidates)

    def from_municipal_or_gazetteer(key):
        try:
            return municipal_map[name][key]
        except KeyError:
            return sorted(name_annotation_map[name][key])

    annotations = [
        {
            'toponym': name,
            'postings': postings,
            'features': from_municipal_or_gazetteer('features'),
            'districts': from_municipal_or_gazetteer('districts'),
            'neighborhoods': from_municipal_or_gazetteer('neighborhoods'),
        }
        for name, postings in name_postings_map.items()
    ]
    return sorted(annotations, key=itemgetter('toponym'))


def get_districts_and_hoods(municipality_code, toponyms):
    features_by_name = {}

    districts = db_collections.district_highover.aggregate([
        {
            '$match': {
                'known_names': {'$in': toponyms},
                'properties.GM_CODE': municipality_code
            }
        },
        {  # for each known name in the pipe
            '$unwind': '$known_names'
        },
        {
            '$project': {
                '_id': 0,
                'toponym': '$known_names',
                'features': [],
                'districts': ['$properties.WK_CODE'],
                'neighborhoods': [],
            }
        },
    ])
    for district in districts:
        features_by_name[district['toponym']] = district

    neighborhoods = db_collections.neighborhood_highover.aggregate([
        {
            '$match': {
                'known_names': {'$in': toponyms},
                'properties.GM_CODE': municipality_code
            }
        },
        {  # for each known name in the pipe
            '$unwind': '$known_names'
        },
        {
            '$project': {
                '_id': 0,
                'toponym': '$known_names',
                'features': [],
                'districts': ['$properties.WK_CODE'],
                'neighborhoods': ['$properties.BU_CODE'],
            }
        },
    ])
    for hood in neighborhoods:
        features_by_name[hood['toponym']] = hood

    return features_by_name


def get_features_per_neighborhood(municipality_code, toponyms):
    neighborhood_regex = '^BU{}'.format(municipality_code[2:])
    return Feature.collection.aggregate([
        {
            '$match': {
                'known_names': {'$in': toponyms},
                'municipalities': municipality_code
            }
        },
        {   # for each neighborhood ref in the pipe
            '$unwind': '$neighborhoods'
        },
        {   # only neighborhoods in this municipality
            '$match': {
                'neighborhoods': {'$regex': neighborhood_regex}
            }
        },
        {
            '$group': {
                '_id': '$neighborhoods',
                'toponyms': {'$push': '$known_names'},
                'features': {'$push': '$_id'},
            }
        },
        {   # add the neighborhood document to this representation
            '$lookup': {
                'from': 'neighborhood_highover',
                'localField': '_id',
                'foreignField': 'properties.BU_CODE',
                'as': 'neighborhood',
            }
        },
        {   # replace the neighborhood doc with adjacent districts and hoods
            '$project': {
                'toponyms': 1,
                'features': 1,
                'feature_count': {'$size': '$features'},
                'adjacent_neighborhoods': {
                    '$arrayElemAt': ['$neighborhood.adjacent_neighborhoods', 0]
                },
                'adjacent_districts': {
                    '$arrayElemAt': ['$neighborhood.adjacent_districts', 0]
                },
            }
        },
        {
            '$sort': {
                'feature_count': -1,
                '_id': 1,
            }
        },
    ])


def select_hoods_and_features(features_per_neighborhood):
    selected_hood_ids = set()
    name_annotation_map = defaultdict(annotation_factory)
    hood_selection_map = defaultdict(list)

    def select_hood(hood):
        selected_hood_ids.add(hood['_id'])
        for i, toponyms in enumerate(hood['toponyms']):
            for name in toponyms:
                name_annotation_map[name]['toponym'] = name
                name_annotation_map[name]['features'].add(str(hood['features'][i]))
                name_annotation_map[name]['districts'].add('WK{}'.format(hood['_id'][2:-2]))
                name_annotation_map[name]['neighborhoods'].add(hood['_id'])

    for hood in features_per_neighborhood:
        if hood['feature_count'] > 1:
            # more than one feature: select
            select_hood(hood)
        elif all(name in name_annotation_map
                 for toponyms in hood['toponyms']
                 for name in toponyms):
            # all names are covered: leave unselected
            pass
        else:
            # needs disambiguation / further selection
            for toponyms in hood['toponyms']:
                for name in toponyms:
                    hood_selection_map[name].append(hood)

    for toponym, hoods in hood_selection_map.items():
        if len(hoods) == 1:
            select_hood(hoods[0])
        else:
            relation_counts = [
                count_relations_with_selected_hoods(hood, selected_hood_ids)
                for hood in hoods
            ]
            total_counts = (
                float(sum(map(itemgetter(0), relation_counts))),
                float(sum(map(itemgetter(1), relation_counts))),
                float(sum(map(itemgetter(2), relation_counts)))
            )
            normalized_counts = [
                fractions_of_total(counts, total_counts)
                for counts in relation_counts
            ]
            max_counts = max(normalized_counts)
            for i, counts in enumerate(normalized_counts):
                if is_close_to_max(counts, max_counts):
                    select_hood(hoods[i])

    return dict(name_annotation_map)


def count_relations_with_selected_hoods(hood, selected_hood_ids):
    adjacent_hoods = sum(
        1 for hood_id in hood['adjacent_neighborhoods']
        if hood_id in selected_hood_ids
    )
    shared_districts = sum(
        1 for hood_id in selected_hood_ids
        if hood['_id'][6:8] == hood_id[6:8]
    )
    adjacent_districts = sum(
        1 for district_id in hood['adjacent_districts']
        for hood_id in selected_hood_ids
        if district_id[-2:] == hood_id[6:8]
    )
    return adjacent_hoods, shared_districts, adjacent_districts


def fractions_of_total(counts_tuple, total_counts):
    return tuple(
        counts_tuple[i] / total_counts[i] if total_counts[i] else 0.0
        for i in range(3)
    )


def is_close_to_max(counts, max_counts, index=0):
    if not any(max_counts):
        # max is all zeroes: select
        return True

    if not max_counts[index]:
        return is_close_to_max(counts, max_counts, index=1 + index)

    if max_counts[index] - counts[index] <= MIN_HOOD_SIMILARITY:
        return True
    else:
        return False


def annotation_factory():
    return {
        'toponym': '',
        'postings': [],
        'features': set(),
        'districts': set(),
        'neighborhoods': set(),
    }

