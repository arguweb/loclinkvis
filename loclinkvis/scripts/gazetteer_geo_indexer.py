#! /usr/bin/env python2

from __future__ import print_function

import itertools
import logging
from operator import attrgetter

from shapely.geometry import mapping, box, shape

from loclinkvis.gazetteer.models import Feature
from loclinkvis.municipal import db_collections

logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
    datefmt='%m-%d %H:%M',
    filename='logs/gazetteer_geo_indexer.log',
    filemode='w'
)
console = logging.StreamHandler()
console.setLevel(logging.INFO)
logging.getLogger('').addHandler(console)
logger = logging.getLogger(__name__)


def features_intersect(geometry):
    return {
        'geometry': {'$geoIntersects': {'$geometry': geometry}},
        'properties.admin_level': {'$exists': False},
        'properties.type': {'$ne': 'boundary'},
    }


def get_bounding_box(neighborhood):
    geometry = neighborhood['geometry']
    return mapping(box(*shape(geometry).bounds))


def filter_candidates(candidates):
    grouped = itertools.groupby(candidates, attrgetter('known_names'))
    for key, group in grouped:
        group = list(group)
        for c in group:
            if 'pc_digits' in c['properties']:
                yield c['_id']
                break
        else:
            # nothing with pc_digits found, just yield last
            yield group[-1]['_id']


def index_features_of(neighborhood):
    candidates = Feature.collection.find(
        features_intersect(get_bounding_box(neighborhood))
    ).sort('known_names', 1)
    candidates = filter_candidates(candidates)

    query = features_intersect(neighborhood['geometry'])
    query['_id'] = {'$in': list(candidates)}

    hood_code = neighborhood['properties']['BU_CODE']
    dist_code = neighborhood['properties']['WK_CODE']
    muni_code = neighborhood['properties']['GM_CODE']

    return Feature.collection.update_many(query, {
        '$addToSet': {
            'neighborhoods': hood_code,
            'districts': dist_code,
            'municipalities': muni_code,
        },
    })


if __name__ == "__main__":
    hood_count = db_collections.neighborhood_highover.count()
    hoods = db_collections.neighborhood_highover.find({}).batch_size(50)

    failures = []

    for index, neighborhood in enumerate(hoods):
        try:
            result = index_features_of(neighborhood)
            logger.info(
                '(%s/%s)\t%d features (%d new) for: %s',
                index, hood_count, result.matched_count,
                result.modified_count, neighborhood['properties']['BU_NAAM'])
        except Exception as e:
            logger.error('ERROR: couldn\'t index %s: %s',
                         neighborhood['properties']['BU_NAAM'], e)
            failures.append((neighborhood['_id'],
                             neighborhood['properties']['BU_NAAM']))

    logger.info('failed neighborhoods (%d):', len(failures))
    for f in failures:
        logger.info('%s: %s', *f)
