#!/bin/sh
ogr2ogr -f "GeoJSON" -s_srs EPSG:28992 -t_srs CRS:84 -simplify 0.15 -lco RFC7946=YES -progress ../../cbs2017/buurt_2017_closeup.json ../../cbs2017/buurt_2017.shp \
&& ogr2ogr -f "GeoJSON" -s_srs EPSG:28992 -t_srs CRS:84 -simplify 0.5 -lco RFC7946=YES -progress ../../cbs2017/buurt_2017_highover.json ../../cbs2017/buurt_2017.shp \
&& ogr2ogr -f "GeoJSON" -s_srs EPSG:28992 -t_srs CRS:84 -simplify 0.5 -lco RFC7946=YES -progress ../../cbs2017/wijk_2017_closeup.json ../../cbs2017/wijk_2017.shp \
&& ogr2ogr -f "GeoJSON" -s_srs EPSG:28992 -t_srs CRS:84 -simplify 2 -lco RFC7946=YES -progress ../../cbs2017/wijk_2017_highover.json ../../cbs2017/wijk_2017.shp \
&& ogr2ogr -f "GeoJSON" -s_srs EPSG:28992 -t_srs CRS:84 -simplify 2 -lco RFC7946=YES -progress ../../cbs2017/gem_2017_closeup.json ../../cbs2017/gem_2017.shp \
&& ogr2ogr -f "GeoJSON" -s_srs EPSG:28992 -t_srs CRS:84 -simplify 7 -lco RFC7946=YES -progress ../../cbs2017/gem_2017_highover.json ../../cbs2017/gem_2017.shp
