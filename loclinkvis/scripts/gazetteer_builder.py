#!/usr/bin/env python2
"""
OSM instance to Gazetteer feature import script

If loclinkvis is not on the PYTHONPATH, this
script can be executed as follows:
$ PYTHONPATH=`pwd` /path/to/python scripts/gazetteer_builder.py
"""
import logging
from collections import Counter
from operator import itemgetter

from pymongo.errors import OperationFailure

from loclinkvis.gazetteer.models import Node, Feature, Way, Relation


logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
    datefmt='%m-%d %H:%M',
    filename='logs/gazetteer_builder.log',
    filemode='w'
)
console = logging.StreamHandler()
console.setLevel(logging.INFO)
logging.getLogger('').addHandler(console)
logger = logging.getLogger(__name__)


name_and_wiki = {
    '$or': [
        {'tags.wikipedia': {'$exists': True}},
        {'tags.wikidata': {'$exists': True}}
    ],                       # exclude names starting with some non-alpha characters
    'tags.name': {'$regex': u'^[^\u0001-\u0026\u0028-\u002f\u003a-\u003f\u005b-\u005f\u007b-\u00bf]'},
}


def count_provenance():
    all_provenance = Feature.collection.distinct('provenance')
    lettergetter = itemgetter(0)
    return Counter(map(lettergetter, all_provenance))


node_count = Node.collection.find(name_and_wiki).count()
if node_count > count_provenance().get('n', 0):
    logger.info('Started loading {} nodes'.format(node_count))
    for node in Node.collection.find(name_and_wiki):
        try:
            Feature.create_or_update_from_osm(node)
        except OperationFailure:
            logger.exception('Failed to create a feature for node/{}'.format(node._id))
else:
    logger.info('Skipping node loading: already in gazetteer')

named_way = name_and_wiki.copy()
named_way['$or'] = named_way['$or'] + [{'tags.highway': {'$exists': True}}]
way_count = Way.collection.find(named_way).count()
if way_count > count_provenance().get('w', 0):
    logger.info('Started loading {} ways'.format(way_count))
    for way in Way.collection.find(named_way).batch_size(50):
        try:
            Feature.create_or_update_from_osm(way, merge_homonyms=True)
        except OperationFailure:
            logger.exception('Failed to create a feature for way/{}'.format(way._id))
else:
    logger.info('Skipping way loading: already in gazetteer')

relation_count = Relation.collection.find(name_and_wiki).count()
wiggle_room = 0.89
if (wiggle_room * relation_count) > count_provenance().get('r', 0):
    logger.info('Started loading {} relations'.format(relation_count))
    for relation in Relation.collection.find(name_and_wiki).batch_size(50):
        try:
            Feature.create_or_update_from_osm(relation)
        except OperationFailure:
            logger.exception('Failed to create a feature for relation/{}'.format(relation._id))
else:
    logger.info('Skipping relation loading: at least {:.0%} in gazetteer'.format(wiggle_room))
