#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Run a Flask server .
"""
import logging
from logging.config import dictConfig

from loclinkvis import settings
from loclinkvis.web_app import app


# Configure logging
dictConfig({
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': '%(levelname)s in %(name)-12s: %(message)s'
        },
        'timed': {
            'format': '[%(asctime)s] %(levelname)s in %(name)-12s: %(message)s',
            'datefmt': '%m-%d %H:%M'
        }
    },
    'handlers': {
        'stream': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'default'
        },
        'file': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'timed',
            'filename': '../logs/webserver.log',
            'mode': 'a',
            'maxBytes': 67000,
            'backupCount': 5,
        },
    },
    'root': {
        'level': 'INFO',
        'handlers': ['stream', 'file']
    }
})

# Flask views
views_loaded = False
if settings.SERVE_GEO_DEMO is True:
    import loclinkvis.geo_demo.views
    views_loaded = True

if settings.SERVE_MUNICIPAL is True:
    import loclinkvis.municipal.views
    views_loaded = True

if not views_loaded:
    app.logger.warning(
        'The webserver is being run without any loaded views. '
        'Make sure that at least one SERVE_* setting is True.'
    )


if __name__ == '__main__':
    for handler in logging.getLogger('').handlers:
        app.logger.addHandler(handler)
    app.logger.info([
        (rule.rule, rule._regex.pattern)
        for rule in app.url_map.iter_rules()
        if rule.endpoint != 'static'
    ])
    app.run(host='0.0.0.0', port=8080)
