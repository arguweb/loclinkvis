from flask import Flask
from flask_cors import CORS

# Initialize a Flask instance
app = Flask(__name__, static_folder='geo_demo/static')
app.url_map.strict_slashes = False

# Cross-origin
CORS(app)
