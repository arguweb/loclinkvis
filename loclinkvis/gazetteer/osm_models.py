#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""The code in this file is derived from https://github.com/larroy/osmcompiler
   It is reused under the terms of the GNU AGPL v3 (http://www.gnu.org/licenses/agpl-3.0.html)
   The original work and the reused portions are © Pedro Larroy Tovar.
"""

# load mongodb credentials
import minimongo
from loclinkvis import mongocredentials

minimongo.configure(module=mongocredentials)


class Node(minimongo.Model):
    def __init__(self, data=None):
        if data:
            super(Node, self).__init__(data)
        else:
            self._id = 0
            self.lon = 0.0
            self.lat = 0.0
            self.version = 0
            self.time = 0
            self.uid = 0
            self.user = ""
            self.changeset = 0

    def addTag(self, k, v):
        ek = mongo_legal_key_escape(k)
        try:
            self.tags[ek] = v
        except AttributeError:
            self.tags = {}
            self.tags[ek] = v

    def __str__(self):
        res = []
        res.append(u'Node {0}: ({1}, {2})\n'.format(self._id, self.lat, self.lon))
        for t in self.tags.keys():
            res.append(u'\t{0} = {1}\n'.format(t, self.tags[t]))
        return u''.join(res)


class Way(minimongo.Model):
    def __init__(self, data=None):
        if data:
            super(Way, self).__init__(data)
        else:
            self._id = 0
            self.time = 0
            self.uid = 0
            self.user = ""
            self.changeset = 0
            self.tags = {}
            self.nodes = []

    def addTag(self, k, v):
        ek = mongo_legal_key_escape(k)
        self.tags[ek] = v

    def addNode(self, nodeid):
        self.nodes.append(nodeid)

    def __str__(self):
        res = [u'Way {0}:\n\tnodes:'.format(self._id)]
        res.append(u', '.join(map(lambda x: str(x), self.nodes)))
        res.append('\n')
        for t in self.tags.keys():
            res.append(u'\t{0} = {1}\n'.format(t, self.tags[t]))
        return u''.join(res)


class Member(minimongo.Model):
    def __init__(self, data=None):
        self.type = ""
        self.ref = 0
        self.role = ""

    def __str__(self):
        return u'Member {0}, {1}, {2}'.format(self.type, self.ref, self.role)


class Relation(minimongo.Model):
    def __init__(self, data = None):
        if data:
            super(Relation, self).__init__(data)
        else:
            self._id = 0
            self.time = 0
            self.uid = 0
            self.user = ""
            self.changeset = 0
            self.tags = {}
            self.members = []

    def addTag(self, k, v):
        ek = mongo_legal_key_escape(k)
        self.tags[ek] = v

    def addMember(self, member):
        self.members.append(member)

    def __str__(self):
        res = [u'Relation {0}:\n\t'.format(self._id)]
        res.append(u'\n\t'.join(map(lambda x: str(x), self.members)))
        return u''.join(res)


def mongo_legal_key_escape(s):
    """Make sure a string is a legal mongo key name, substitute unsafe characters"""
    key_chars = []
    for c in s:
        if c == '%':
            key_chars.append('%%')

        elif c == '~':
            key_chars.append('~~')

        elif c == '^':
            key_chars.append('^^')

        elif c == '$':
            key_chars.append('%~')

        elif c == '.':
            key_chars.append('%^')

        else:
            key_chars.append(c)
    return ''.join(key_chars)


def mongo_legal_key_unscape(s):
    key_chars = []
    skipnext = False
    for (i,cur) in enumerate(s):
        next_char = None
        if skipnext:
            skipnext = False
            continue

        if i+1 < len(s):
            next_char = s[i+1]
        if cur == '%':
            if next_char:
                if next_char == '~':
                    key_chars.append('$')
                    skipnext = True

                elif next_char == '%':
                    key_chars.append('%')
                    skipnext = True

                elif next_char == '^':
                    key_chars.append('.')
                    skipnext = True

                else:
                    raise RuntimeError('unscaping unscaped string')

        elif cur == '~':
            if next_char:
                if next_char == '~':
                    key_chars.append('~')
                    skipnext = True

                else:
                    raise RuntimeError('unscaping unscaped string')

        elif cur == '^':
            if next_char:
                if next_char == '^':
                    key_chars.append('^')
                    skipnext = True

                else:
                    raise RuntimeError('unscaping unscaped string')

        else:
            key_chars.append(cur)

    return ''.join(key_chars)
