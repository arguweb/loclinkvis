#!/usr/bin/env python
# -*- coding: utf-8 -*-
import csv
import logging
import re
from collections import defaultdict

import telwoord
from pymongo.errors import DuplicateKeyError

from models import Nominatim, Feature, classmap


logger = logging.getLogger(__name__)
suffix_re = re.compile(r'^[A-Z].{0,2}$')

"""
200k streets in csv
"""


def process_kb_streets(streets_csv_path, do_fetch=True, to_gazetteer=True):
    telwoord_ordinal = {}
    for n in range(21):
        telwoord_ordinal[telwoord.ordinal(n).capitalize()] = '{}e'.format(n)

    with open(streets_csv_path, 'r') as f:
        reader = csv.reader(f)
        result_count = 0
        not_found = []
        not_into_gazetteer = []
        last_postal_code = None
        for row in reader:
            try:
                row = [cell.decode('utf8') for cell in row]
                if row[0] != last_postal_code:
                    last_postal_code = row[0]
                    logger.info(last_postal_code)

                # swap town and road if town starts with Röntgen
                if row[1].startswith(u'R\xf6ntgen'):
                    road = row[1]
                    town = row[2]
                else:
                    town = row[1]
                    road = row[2]

                # split the road name into tokens
                road_split = road.split()
                # resolve leading numerals into their integer (+e) form
                if road_split[0] in telwoord_ordinal:
                    road_split[0] = telwoord_ordinal[road_split[0]]
                    road = ' '.join(road_split)
                # strip any awkward street name suffixes
                if suffix_re.match(road_split[-1]):
                    road = ' '.join(road_split[:-1])

                # FIXME: 200k queries
                nres = Nominatim.get_res_by_address(city=town, street=road)
                if not nres and do_fetch:
                    # FIXME: worst case 200k queries
                    nres = Nominatim.query(
                        city=town, street=road
                    )
                    # try abbreviating leading token(s)
                    if not nres and len(road_split) > 2:
                        road = ' '.join(
                            [road_split[0]] + [
                                rn[0] if rn.istitle() else rn
                                for rn in road_split[1:-1]
                            ] + [road_split[-1]]
                        )
                        # FIXME: worst case 200k queries
                        nres = Nominatim.query(
                            city=town, street=road
                        )
                    if not nres and len(road_split) > 1:
                        road = ' '.join(
                            [
                                rn[0] if rn.istitle() else rn
                                for rn in road_split[:-1]
                            ] + [road_split[-1]]
                        )
                        # FIXME: worst case 200k queries
                        nres = Nominatim.query(
                            city=town, street=road
                        )
                if not nres:
                    not_found.append(row)
                    logger.info('Not found: {} {}'.format(row[:3], nres))
                else:
                    result_count += 1
                    for ndoc in nres:
                        if do_fetch:
                            try:
                                # FIXME: n*200k queries worst case
                                Nominatim(ndoc).save()
                            except DuplicateKeyError:
                                pass

                        logger.debug(ndoc['display_name'])

                # Ensure there is a feature for exact town, road matches
                if to_gazetteer:
                    # FIXME: 200k queries
                    nres = Nominatim.get_res_by_address(city=town, street=road)
                    if not nres:
                        not_into_gazetteer.append(row)
                        logger.warning('Not loaded into gazetteer: {} {}'.format(row[:3], nres))
                    else:
                        ndoc_per_type = defaultdict(list)
                        for ndoc in nres:
                            ndoc_per_type[ndoc.osm_type].append(ndoc)

                        for osm_type in ('node', 'way', 'relation'):
                            for ndoc in sorted(ndoc_per_type[osm_type], key=pc_sort_key):
                                ElementClass = classmap[ndoc.osm_type]
                                # FIXME: n*600k queries
                                element = ElementClass.collection.find_one({'_id': int(ndoc.osm_id)})
                                if element:
                                    if not element.tags.get('name'):
                                        element.tags.name = ndoc.get_name_from_address()
                                    element.tags.pc_digits = ndoc.get_postcode_digits()
                                else:
                                    try:
                                        element = ndoc.to_mock_instance()
                                    except AttributeError:
                                        logger.warning('Nominatim result does not contain geojson {}'.format(ndoc))
                                        continue
                                # FIXME: ????k queries
                                Feature.create_or_update_from_osm(element)

            except Exception:
                logger.exception('Could not process street')
                if to_gazetteer:
                    not_into_gazetteer.append(row)
                continue

    logger.info('{} non-null responses'.format(result_count))
    logger.info('{} queries yielded no result'.format(len(not_found)))
    with open('logs/not_found.csv', 'a') as f:
        csvw = csv.writer(f, delimiter=',')
        for row in not_found:
            csvw.writerow([cell.encode('utf8') for cell in row])
    with open('logs/not_into_gazetteer.csv', 'a') as f:
        csvw = csv.writer(f, delimiter=',')
        for row in not_into_gazetteer:
            csvw.writerow([cell.encode('utf8') for cell in row])
    return not_found


def pc_sort_key(ndoc):
    return ndoc.address.postcode.replace(' ', '')
