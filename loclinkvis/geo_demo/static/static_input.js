var app = angular.module('reviewApp',['ui.bootstrap']);

app.controller("StatementCtrl", function ($scope, $timeout, $http, $location) {
    $scope.initdata = function() {
        // Counter for spotted names
        $scope.spottedNames = {};
        $scope.spottedNames.get = function(name){
            if ($scope.spottedNames.hasOwnProperty(name)) return $scope.spottedNames[name];
            else return 0
        }
        
        // Initialize scope data from server
        $scope.pmId = $location.path();
        $http.get('/features_from_static' + $scope.pmId).success(function(resp){
            $scope.map = L.map('map').setView([52.253, 5.765], 8);
            $scope.tileLayer = L.tileLayer('https://{s}.tiles.mapbox.com/v3/{id}/{z}/{x}/{y}.png', {
                maxZoom: 18,
                attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
                    '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                    'Imagery &copy; <a href="http://mapbox.com">Mapbox</a>',
                id: 'examples.map-20v6611k'
            }).addTo($scope.map);
            $scope.featureLayer = L.geoJson().addTo($scope.map);
            Object.keys(resp.spottedNames).forEach(function(key) {
                $scope.spottedNames[key] = $scope.spottedNames.get(key) + resp.spottedNames[key]
            });
            $scope.featureLayer.addData(resp.featureCollection);

            $('#loader').fadeOut(513, function(){
                $('#loader').remove();
            });
        }).error(function(response, status){
                $('#loadmsg').text("Loading Failed: HTTP Status "+status)
        });
    };

    /* Get features and add them to the map */
    $scope.getFeatureByName = function(toadd){
        $http.get('/location/' + toadd).success(function(featureCollection){
            $scope.featureLayer.addData(featureCollection);
        });
        $scope.selected = "";
    };
    $scope.getFeaturesFromText = function(text){
        $http.post('/features_from_text', {'text': text}).success(function(resp){
            Object.keys(resp.spottedNames).forEach(function(key) {
                $scope.spottedNames[key] = $scope.spottedNames.get(key) + resp.spottedNames[key]
            });
            $scope.featureLayer.addData(resp.featureCollection);
            $scope.inputText = "";
        });
    };
});
